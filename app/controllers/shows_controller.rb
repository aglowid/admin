class ShowsController < BaseController
  before_action :set_show, only: [:show, :edit, :update, :destroy]

  # GET /shows
  # GET /shows.json
  def index
    @shows = Show.all.order('start DESC')
  end

  # GET /shows/1
  # GET /shows/1.json
  def show
  end

  # GET /shows/new
  def new
    @show = Show.new
  end

  # GET /shows/1/edit
  def edit
  end

  # POST /shows
  # POST /shows.json
  def create
    @show = Show.new(show_params)

    respond_to do |format|
      @start = DateTime.new(params[:show]["date(1i)"].to_i, params[:show]["date(2i)"].to_i, params[:show]["date(3i)"].to_i, params[:show]["start(4i)"].to_i, params[:show]["start(5i)"].to_i)

      @end = DateTime.new(params[:show]["date(1i)"].to_i, params[:show]["date(2i)"].to_i, params[:show]["date(3i)"].to_i, params[:show]["end(4i)"].to_i, params[:show]["end(5i)"].to_i)

      @show.start = @start
      @show.end = @end

      if @show.save
        format.html { redirect_to :shows, notice: 'Show was successfully created.' }
        format.json { render :show, status: :created, location: @show }
      else
        format.html { render :new }
        format.json { render json: @show.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /shows/1
  # PATCH/PUT /shows/1.json
  def update
    respond_to do |format|
      if @show.update(show_params)
        #create custom date out of the date and the start params
        @start = DateTime.new(params[:show]["date(1i)"].to_i, params[:show]["date(2i)"].to_i, params[:show]["date(3i)"].to_i, params[:show]["start(4i)"].to_i, params[:show]["start(5i)"].to_i)

        @end = DateTime.new(params[:show]["date(1i)"].to_i, params[:show]["date(2i)"].to_i, params[:show]["date(3i)"].to_i, params[:show]["end(4i)"].to_i, params[:show]["end(5i)"].to_i)

        @show.update(start: @start, end: @end)

        format.html { redirect_to :shows, notice: 'Show was successfully updated.' }
        format.json { render :show, status: :ok, location: @show }
      else
        format.html { render :edit }
        format.json { render json: @show.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /shows/1
  # DELETE /shows/1.json
  def destroy
    if @show.destroy
      redirect_to shows_url, flash: { warning: 'Show was deleted.' }
    else
      msg = if @show.errors.present?
        @show.errors.full_messages.join(".\n") + "."
      else "Show could not be deleted." end
      redirect_to @show, alert: msg
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_show
      @show = Show.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def show_params
      params.require(:show).permit(:location_id, :date, :start, :end, :early_bird_price,
        :online_price, :door_price, :wine_tasting_price, :early_bird_end_date, :prize_ribbons)
    end
end
