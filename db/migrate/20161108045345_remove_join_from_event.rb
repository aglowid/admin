class RemoveJoinFromEvent < ActiveRecord::Migration[5.0]
  def change
    remove_column :events, :join, :string
  end
end
