class AddJoinToEvent < ActiveRecord::Migration[5.0]
  def change
    add_column :events, :join, :boolean
  end
end
